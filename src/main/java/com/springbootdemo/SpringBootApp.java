package com.springbootdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

/**
 * @author CJY
 * @date 2021-02-02
 **/
@EnableAutoConfiguration
public class SpringBootApp {

    public static void main(String[] args){
        SpringApplication.run(SpringBootApp.class);
    }
}
