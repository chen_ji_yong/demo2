package com.springbootdemo;

import com.github.chenlijia1111.utils.oauth.jwt.JWTUtil;
import io.jsonwebtoken.Claims;

/**
 * @author CJY
 * @date 2021-02-02
 **/
public class Demo {

    public static void main(String[] args) {
       /* String jwt = JWTUtil.createJWT("1","abc",60*60*1000L,"fgh123");
        System.out.println(jwt);*/
        Claims claims = JWTUtil.parseJWT("eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIxIiwiaWF0IjoxNjEyMjU4OTc1LCJzdWIiOiJhYmMiLCJleHAiOjE2MTIyNjI1NzV9.B5HKZlowI73Dl3Lf3bwFAX8MFh3paHEth1Py-yuX3Fc","fgh123");
        System.out.println(claims.getId());
        System.out.println(claims.getExpiration());
    }



}
